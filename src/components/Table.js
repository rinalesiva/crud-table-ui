import React, { useState, useEffect } from "react";
import Row from "./Row";

const Table = () => {
	const [error, setError] = useState(null);
	const [isLoaded, setIsLoaded] = useState(false);
	const [data, setData] = useState([]);
	const [status, setStatus] = useState([]);
	const [isNew, setIsNew] = useState([]);

	useEffect(() => {
		fetch("http://178.128.196.163:3000/api/records")
			.then((response) => {
				if (response.status >= 200 && response.status <= 299) {
					return response.json();
				} else {
					throw new Error("Server responds with error!");
				}
			})
			.then(
				(data) => {
					const processedData = data.filter((obj) => "data" in obj === true);
					const readStatus = processedData.map(() => {
						return true;
					});
					const cIsNew = processedData.map(() => {
						return false;
					});
					setData(processedData);
					setIsLoaded(true);
					setStatus(readStatus);
					setIsNew(cIsNew);
				},
				(error) => {
					setIsLoaded(true);
					setError(error);
				}
			)
			.catch((error) => {
				console.error("There was an error!", error);
			});
	}, []);

	const updateObj = (ind, obj) => {
		const cData = data.slice();
		cData[ind].data = obj;

		const requestOptions = {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({ data: cData[ind].data }),
		};
		fetch(
			"http://178.128.196.163:3000/api/records/" + cData[ind]._id,
			requestOptions
		)
			.then((response) => {
				if (response.status >= 200 && response.status <= 299) {
					return response.json();
				} else {
					throw new Error("Server responds with error!");
				}
			})
			.then((response) => {
				if (typeof response === "object") {
					if ("data" in response) {
						cData[ind].data = response.data;
						setData(cData);
					}
				}
			})
			.catch((error) => {
				console.error("There was an error!", error);
			});
	};

	const deleteObj = (ind, obj) => {
		let cData = data.slice();
		let cStatus = status.slice();
		let cIsNew = isNew.slice();
		const requestOptions = {
			method: "DELETE",
			headers: { "Content-Type": "application/json" },
		};
		fetch(
			"http://178.128.196.163:3000/api/records/" + data[ind]._id,
			requestOptions
		)
			.then((response) => {
				if (response.status >= 200 && response.status <= 299) {
					return response.json();
				} else {
					throw new Error("Server responds with error!");
				}
			})
			.then((response) => {
				if (response) {
					cData.splice(ind, 1);
					cStatus.splice(ind, 1);
					cIsNew.splice(ind, 1);

					setData(cData);
					setStatus(cStatus);
					setIsNew(cIsNew);
				}
			})
			.catch((error) => {
				console.error("There was an error!", error);
			});
	};

	const putData = (obj, ind) => {
		const jsonData = { data: obj };
		const requestOptions = {
			method: "PUT",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(jsonData),
		};
		fetch("http://178.128.196.163:3000/api/records", requestOptions)
			.then((response) => {
				if (response.status >= 200 && response.status <= 299) {
					return response.json();
				} else {
					throw new Error("Server responds with error!");
				}
			})
			.then(
				(response) => {
					const cData = data.slice();
					const cStatus = status.slice();
					const cIsNew = isNew.slice();

					for (let [key] of Object.keys(cData[ind])) {
						cData[ind][key] = response[key];
					}
					cStatus[ind] = true;
					cIsNew[ind] = false;
					setData(cData);
					setStatus(cStatus);
					setIsNew(cIsNew);
					setIsLoaded(true);
				},
				(error) => {
					setIsLoaded(true);
					setError(error);
				}
			)
			.catch((error) => {
				console.error("There was an error!", error);
			});
	};

	const addElement = () => {
		// const newEl = data.slice()[0];
		const newEl = { _id: guidGenerator(), data: {} };

		if ("data" in newEl) {
			for (let [key] of Object.keys(data[0].data)) {
				newEl.data[key] = "empty";
			}
			const cStatus = status;
			const cData = data.slice();
			const cIsNew = isNew.slice();

			cStatus.push(true);
			cData.push(newEl);
			cIsNew.push(true);

			setStatus(cStatus);
			setData(cData);
			setIsNew(cIsNew);
		}
	};

	// const { error, isLoaded, data } = this.state;

	if (error) {
		return <div>Ошибка: {error.message}</div>;
	} else if (!isLoaded) {
		return <div>Загрузка...</div>;
	} else {
		let rows = [];
		let headers = [];

		if (data.length > 0) {
			rows = data.map((obj, ind) => {
				if ("data" in obj) {
					if (headers.length < 1) {
						for (let [key] of Object.keys(obj.data)) {
							headers.push(<th key={key}>{key}</th>);
						}
					}
					return (
						<Row
							key={obj._id}
							data={obj.data}
							index={ind}
							readOnly={!status[ind]}
							isNew={isNew[ind]}
							updateObj={(ind, obj) => {
								updateObj(ind, obj);
							}}
							deleteObj={(ind, obj) => {
								deleteObj(ind, obj);
							}}
							putFunction={(obj) => putData(obj, ind)}
						/>
					);
				}
			});

			headers.push(<th key="del"></th>);
			headers.push(<th key="edit"></th>);
		} else {
			return <div>В загруженных данных ничего нет!</div>;
		}

		return (
			<div>
				<table className="App">
					<tbody>
						<tr>{headers}</tr>
						{rows}
					</tbody>
				</table>
				<button onClick={() => addElement()}>Add element</button>
			</div>
		);
	}
};

function guidGenerator() {
	var S4 = function () {
		return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
	};
	return (
		S4() +
		S4() +
		"-" +
		S4() +
		"-" +
		S4() +
		"-" +
		S4() +
		"-" +
		S4() +
		S4() +
		S4()
	);
}

export default Table;
